import json

import qi

json_folder = "json"
api_folder = "api"

# For now it's hardcoded but in future when meta will send all config, it will be not
heatbeat_ALMemory = {
    'tablet': "R2019/Global/JsHearbeat",
    'lm': "R2019/Global/LMHearbeat",
    'gm': "R2019/Global/GMHearbeat",
}


class Starter(object):
    lm_ok = False
    gm_ok = False
    tablet_ok = False
    need_to_start  = True

    def __init__(self):

        url = "tcp://" + "127.0.0.1" + ":9559"

        self.session = qi.Session()
        self.session.connect(url)
        self.memory = self.session.service("ALMemory")
        self.callback_ids = {}

        self.attach_event(heatbeat_ALMemory['tablet'], self.handle_tablet_heartbeat)
        self.attach_event(heatbeat_ALMemory['lm'], self.handle_lm_heartbeat)
        self.attach_event(heatbeat_ALMemory['gm'], self.handle_gm_heartbeat )

    def handle_tablet_heartbeat(self, value=None):
        Starter.tablet_ok = True
        self.check_all("JS")

    def handle_lm_heartbeat(self, value=None):
        Starter.lm_ok = True
        self.check_all("LM")

    def handle_gm_heartbeat(self, value=None):
        Starter.gm_ok = True
        self.check_all("GM")

    def attach_event(self, event_name, callback):
        try:
            self.callback_ids[event_name] = self.memory.subscriber(event_name)
            self.callback_ids[event_name].signal.connect(callback)
        except Exception as ex:
            raise ex

    def check_all(self, frome):
        if Starter.need_to_start:
            print ("Called from ", frome)
            print(Starter.tablet_ok, Starter.gm_ok, Starter.lm_ok)
            if Starter.tablet_ok and Starter.gm_ok and Starter.lm_ok:
                print("Ask GM to start scenario")
                self.memory.raiseEvent("R2019/Global/GM_Status", json.dumps({'status': True}))
                Starter.need_to_start = False
            pass

if __name__ == '__main__':
    Starter()
    raw_input("Press a key to exit")