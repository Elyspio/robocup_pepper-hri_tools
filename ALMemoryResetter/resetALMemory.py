#!/usr/bin/env python
# -*- encoding: UTF-8 -*-

import argparse

import qi

scenarios_folder = "json"
api_folder = "api"

logging = True

parser = argparse.ArgumentParser(description='Fake general manager.')
parser.add_argument("--scenario", type=str, default="receptionist")
parser.add_argument("--start-at", type=int, default=0, help="Start the general manager at the action with this id")

args = parser.parse_args()


def log(*params):
    if logging:
        print params


class GeneralManager(object):

    def __init__(self):
        url = "tcp://" + "127.0.0.1" + ":9559"

        self.session = qi.Session()
        self.session.connect(url)
        self.memory = self.session.service("ALMemory")
        no_removed = 0
        removed = 0
        for name in (name for name in self.memory.getDataListName() if "R2019" in name):
            try:
                self.memory.removeData(name)
                print ("Removed " + str(name))
                removed += 1

            except Exception as e:
                print ("Cant remove " + str(name))
                no_removed += 1

        print ("\nNb Removed " + str(removed))
        print ("Nb Not Removed " + str(no_removed))


if __name__ == '__main__':
    GeneralManager()
