#!/usr/bin/env python

import argparse
import os

from PIL import Image
import colorama
from colorama import Fore
from resizeimage import resizeimage

colorama.init()

parser = argparse.ArgumentParser("ImageFormatter")

parser.add_argument("--image", "-i", dest="isImage", help="Flag for process images", action="store_true")
# parser.add_argument("--video", "-v", dest="isVideo", help="Flag for process video", action="store_true")

parser.add_argument("--input",
                    dest="input",
                    help="Input folder for raw images")

parser.add_argument("--output",
                    dest="output",
                    help="Output folder for resized images")

parser.add_argument("-f", "--format",
                    dest="format",
                    help="Format for resized images",
                    choices=["png", "jpg"])
parser.add_argument("-t", "--type",
                    dest="type",
                    help="If it's for icons (drinks, face) or as background picture",
                    choices=["large", "small"])


def is_an_image(file_name):
    """
    :type file_name str
    :param file_name: the name of the file that is checked
    :return: True if the file_name has the
    good extension and the index of the position of the extension ".png" in file_name, False otherwise.
    """
    possible_type = [".jpg", ".png", '.jpeg']
    for current_type in possible_type:
        if current_type in file_name:
            return True, file_name.index(current_type)
    return False, None


def format_images(args):
    if not args['format']:
        print("No " + Fore.RED + "format specified " + Fore.RESET + ", please use flag "
              + Fore.CYAN + "--format [format]" + Fore.RESET)
    else:
        raw_images_folder = os.path.join(os.getcwd(), args['input'])
        formatted_images_folder = os.path.join(os.getcwd(), args['output'])

        if not os.path.exists(raw_images_folder):
            print(
                    Fore.RED + "No Input folder " + raw_images_folder + " insert your images in and restart the script." + Fore.RESET)
            os.mkdir(raw_images_folder)

        else:
            # List files
            if not os.path.exists(formatted_images_folder):
                print (Fore.YELLOW + "No Output folder " + formatted_images_folder + " it will be created" + Fore.RESET)
                os.mkdir(formatted_images_folder)
            listdir = os.listdir(raw_images_folder)
            nb_images = len(listdir)
            print("\nResizing {0} images\n".format(nb_images))

            for index, file_name in enumerate(listdir):
                success, extension_index = is_an_image(file_name)
                if success:
                    path_to_image = os.path.join(raw_images_folder, file_name)
                    with open(path_to_image, "r+b") as f:
                        with Image.open(f) as image:
                            if args['type'] == "small":
                                cover = resizeimage.resize_cover(image, [200, 200])
                            elif args['type'] == 'logo':
                                cover = resizeimage.resize_cover(image, [200, 100])
                            elif args['type'] == "large":
                                cover = resizeimage.resize_cover(image, [600, 400])
                            cover.save(
                                os.path.join(formatted_images_folder,
                                             file_name[0: extension_index] + "." + args['format']),
                                image.format)
                            print(("File {0:2} / {1:2}\t{2:30}" + Fore.LIGHTGREEN_EX + "resized" + Fore.RESET)
                                  .format(index + 1, nb_images, file_name))

        print("\nResized images are in folder " + Fore.CYAN + args['output'] + Fore.RESET + "\n\n")


if __name__ == '__main__':
    args = parser.parse_args()
    format_images(args)
