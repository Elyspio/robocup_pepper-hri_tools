
import colorama
from colorama import Fore
from format import format_images
import sys
run = True

while run:
    print("\nEnter which picture do you want to resize?")
    print(Fore.RED + "1" + Fore.RESET + " - Drinks")
    print(Fore.RED + "2" + Fore.RESET + " - Location")
    print(Fore.RED + "3" + Fore.RESET + " - Other Images (logo)")
    print("\nAnything else to exit")

    if sys.version_info[0] == 2:
        usr = raw_input("? ")
    else:
        usr = str(input("? "))

    if usr == "1":
        format_images({
            'input': "raw/drinks",
            'output': "resized/drinks",
            'type': 'small',
            'format': "png"
        })
        print("OK")
    elif usr == "2":
        format_images({
            'input': "raw/locations",
            'output': "resized/locations",
            'type': 'large',
            'format': "png"
        })
    elif usr == "3":
        format_images({
            'input': "raw/images",
            'output': "resized/images",
            'type': 'logo',
            'format': "png"
        })
    else:
        run = False
