# Image Formatter

This tool is used to reduce image size and crop them.

## Installation

Use `pip install -r requirements.txt` to fetch dependencies.

## Use

Place your images in a folder (`raw_images` by default)

Run script using `python format.py`
