#!/usr/bin/env
import argparse
import json
import os
import threading
import time

import qi

scenarios_folder = "json"
api_folder = "api"

logging = True

parser = argparse.ArgumentParser(description='Fake general manager.')
parser.add_argument("--scenario", type=str, default="receptionist")
# parser.add_argument("--start-at", type=int, default=0, help="Start the general manager at the action with this id")

args = parser.parse_args()


def log(*params):
    if logging:
        print params


class GeneralManager(object):
    ros_work_done = False
    hri_work_done = False
    current_timeout_done = False

    def __init__(self):
        url = "tcp://" + "127.0.0.1" + ":9559"
        # app = qi.Application(["GM", "--qi-url=" + url])

        with open(os.path.join(api_folder, "common.json")) as common_api:
            with open(os.path.join(api_folder, "generalManagerToHRI.json")) as gmToHri:
                self.apis = {
                    "common": json.load(common_api),
                    "gmToHRI": json.load(gmToHri)
                }

        self.session = qi.Session()
        self.session.connect(url)
        self.memory = self.session.service("ALMemory")
        self.callback_ids = {}
        self.steps = []
        self.curr_step_ind = 0
        self.attach_event("R2019/Global/GM_Status", self.start_gm)

        threading.Thread(target=self.gm_heartbeat).start()
        log("GM is waiting Starter !")

    def start_gm(self, *value):

        print("Starting Gm")

        scenarios = {}
        speech = {}
        for f in os.listdir(scenarios_folder):
            path_to_file = os.path.join(os.getcwd(), scenarios_folder, f)
            if os.path.isdir(path_to_file):
                # here f is a scenario folder
                for inner_file in os.listdir(path_to_file):
                    path_to_scenario_file = os.path.join(path_to_file, inner_file)
                    with open(path_to_scenario_file) as abs_file:
                        if "scenario" in inner_file:
                            scenarios[f] = json.load(abs_file)
                        if "speech" in inner_file:
                            speech[f] = json.load(abs_file)

        for scenario_name in scenarios.keys():
            log('Found scenario: ', scenario_name)

            # print json.dumps(scenarios[scenario_name], indent=4)
            # print json.dumps(speech[scenario_name], indent=4)

        curr_scenario_name = args.scenario

        # check if curr_scenario exist
        if curr_scenario_name in scenarios:
            curr_scenario = scenarios[curr_scenario_name]
            self.steps = curr_scenario["steps"]
            # Change the current scenario
            self.memory.raiseEvent(self.apis["gmToHRI"]["currentScenario"]["ALMemory"],
                                   json.dumps({"scenario": curr_scenario}))

            # Initialize step listener
            self.attach_event(self.apis["gmToHRI"]["actionComplete"]["ALMemory"], self.handle_hri_job_complete)

            # Start the timer
            self.memory.raiseEvent(self.apis["gmToHRI"]["timerState"]["ALMemory"],
                                   json.dumps({"state": self.apis["gmToHRI"]["timerState"]["state"]["on"]}))

            if self.curr_step_ind < len(self.steps):
                self.next_step()

    def gm_heartbeat(self):
        while True:
            self.memory.raiseEvent(self.apis["common"]["generalManagerHeartbeat"]["ALMemory"],
                                   json.dumps({'time': time.time()}))
            time.sleep(1)

    def simulate_ros_work(self, time_for_work):
        time.sleep(time_for_work)
        GeneralManager.ros_work_done = True
        self.handle_common_complete("{}")


    def handle_common_complete(self, value):
        if GeneralManager.current_timeout_done:
            print("DO SOMETHING WITH THIS TIMEOUT action_index: " + str(self.curr_step_ind))
        else:
            if GeneralManager.ros_work_done and GeneralManager.hri_work_done:
                value = json.loads(value)
                if "error" in value:
                    if value["error"] == "confirm":
                        self.curr_step_ind -= 1
                else:
                    self.curr_step_ind += 1
                self.next_step()

    def handle_hri_job_complete(self, value):
        GeneralManager.hri_work_done = True
        log("Ros Js completed")
        self.handle_common_complete(value)

    def next_step(self, timeout=9999):
        if len(self.steps) > self.curr_step_ind:

            curr_step = self.steps[self.curr_step_ind]
            GeneralManager.hri_work_done = GeneralManager.ros_work_done = GeneralManager.current_timeout_done = False
            threading.Thread(target=self.start_timeout(timeout))

            if curr_step["action"] == "":
                log("New major task", curr_step["name"])

                self.memory.raiseEvent(self.apis["gmToHRI"]["stepCompleted"]["ALMemory"], json.dumps({}))

                log("send currentAction", {"actionId": curr_step['id']})
                self.memory.raiseEvent(self.apis["gmToHRI"]["currentStep"]["ALMemory"],
                                       json.dumps({"actionId": curr_step['id']}))

                GeneralManager.hri_work_done = GeneralManager.ros_work_done = True
                self.handle_common_complete("{}")
            else:
                obj = curr_step["arguments"]
                obj["actionId"] = curr_step['id']

                log("Send action with name:", curr_step['name'])
                self.memory.raiseEvent(self.apis["gmToHRI"]["currentAction"]["ALMemory"], json.dumps(
                    obj
                ))

                if curr_step["action"] in ["humanDetection", "goTo", "pointTo", "find", "closeHand", "openHand",
                                           "checkBag", "holdBag"]:
                    log(" for 5 seconds")
                    self.simulate_ros_work(5)
                else:
                    log("ROS for 1 seconds")
                    self.simulate_ros_work(1)

    def attach_event(self, event_name, callback):
        try:
            self.callback_ids[event_name] = self.memory.subscriber(event_name)
            self.callback_ids[event_name].signal.connect(callback)
            log("Attached event " + event_name + " to " +
                callback.__name__)
        except Exception as ex:
            log("Something went wrong while attaching event " +
                event_name + ": " + str(ex))

    def start_timeout(self, timeout):
        time.sleep(timeout)
        GeneralManager.current_timeout_done = True
        self.handle_common_complete({})


if __name__ == '__main__':
    try:
        GeneralManager()
        raw_input("Enter to exit\n")
    except RuntimeError as e:
        print e
