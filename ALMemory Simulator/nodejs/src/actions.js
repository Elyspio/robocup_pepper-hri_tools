const actions = {
    insertData : "setALMemoryData",
    getData :"getData", 
    raiseEvent :"raiseEvent",
    subscribe : "subscribe",
    unsubscribe :"unsubscribe",
    getTimeStamp : "getTimeStamp",
    getDataList :"getDataList",
    }

export const pureJSAction = {
    jobCompleted : "JOB_COMPLETED"
}

export default actions;