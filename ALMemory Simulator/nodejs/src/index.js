import ALMemoryClient from './client'
import ALMemoryServer from './server'

ALMemoryServer.createServer("0.0.0.0", 4242);

export {
    ALMemoryClient,
    ALMemoryServer
}