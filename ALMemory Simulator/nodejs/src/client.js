//@ts-check

import Action, { pureJSAction } from './actions'

import WebSocket from 'ws'

export default class ALMemoryClient {

    /** @type {WebSocket} */
    innerClient;

    /**
     * @type {Map<number, Function>}
     */
    static mapIdResolve = new Map();

    /**
     * @type {Map<string, Set<Function>>}
     */
    mapIdCallback = new Map();

    static nbRequest = 0;
    static nbClient = 0


    // 1023 +33 6 10 00 10 23

    /**
    * @param {string} host
    * @param {number} port
    * @param {string} label 
    */
    constructor(host, port, label = "Client " + ALMemoryClient.nbClient) {
        this.innerClient = new WebSocket(`ws://${host}:${port}`);
        // this.innerClient = net.createConnection({ host: host, port: port });
        this.label = label;

        this.innerClient.on('open', () => {
            console.log("Open");
            this.innerClient.on("message", data => {
                console.log("New Data : ", data);
                // @ts-ignore
                const dataObjArr = data.split('\n');
                /**
                 * @param {string} dataObjRaw
                 */
                dataObjArr.forEach(dataObjRaw => {
                  

                    if (dataObjRaw.includes('{') && dataObjRaw.includes("}")) {
                        const dataObj = JSON.parse(dataObjRaw);
                        console.log("DataObj : ", dataObj);


                        switch (dataObj.action) {

                            case pureJSAction.jobCompleted:
                                // const resolve = ALMemoryClient.mapIdResolve.get(dataObj.resolveId);
                                // ALMemoryClient.mapIdResolve.delete(dataObj.resolveId);
                                // resolve({ message: "Job " + dataObj.id + " is complete" })
                                break;


                            case Action.getData:
                            case Action.getDataList:
                            case Action.getTimeStamp:

                            console.log("Here", ALMemoryClient.mapIdResolve);

                                if (ALMemoryClient.mapIdResolve.has(dataObj.resolveId)) {
                                    const resolve = ALMemoryClient.mapIdResolve.get(dataObj.resolveId);
                                    ALMemoryClient.mapIdResolve.delete(dataObj.resolveId);
                                    resolve(dataObj.value);
                                }

                                break;

                            case Action.raiseEvent:


                                const callback = this.mapIdCallback.get(dataObj.eventName);
                                callback.forEach(cb => {
                                    cb(dataObj.payload);
                                })
                                break
                        }

                    }



                })
            });
        })


        this.innerClient.on("error", error => {
            console.error("Error on client", error);
        })

    }

    /**
     * @returns {Promise<any | undefined>}  return data from the ALMemory named 'ALMemoryName', if this ALMemory is undefined, null is returned 
     * @param {string} ALMemoryName 
     */
    getData(ALMemoryName) {

        const id = ALMemoryClient.nbRequest++;

        this.send(JSON.stringify({
            action: Action.getData,
            ALMemoryName: ALMemoryName,
            resolveId: id
        }) + '\n');

        return new Promise(resolve => {
            ALMemoryClient.mapIdResolve.set(id, resolve);
        })

    }


    /**
     * @param {string} ALMemoryName
     * @param {any} value
     * @returns {Promise}
     */
    insertData(ALMemoryName, value) {

        const id = ALMemoryClient.nbRequest++;
        return new Promise(resolve => {
            this.send(JSON.stringify({
                action: Action.insertData,
                ALMemoryName: ALMemoryName,
                value: value,
                resolveId: id
            }) + '\n', () => {
                resolve();
            });

        })


    }

    /**
     * @param {string} eventName 
     * @param {Function} cb 
     * @returns {Promise<string>} A promise with a message which indicate that subscribe is done.
     * @description Allow client to subscribe to an ALMemory, when this ALMemory is modified by raiseEvent, the callback is run.
     */
    subscribe(eventName, cb) {


        if (!this.mapIdCallback.has(eventName)) {
            this.mapIdCallback.set(eventName, new Set());
        }

        const cbSet = this.mapIdCallback.get(eventName);
        cbSet.add(cb);

        const id = ALMemoryClient.nbRequest++;
        return new Promise(resolve => {

            ALMemoryClient.mapIdResolve.set(id, resolve);

            this.send(JSON.stringify({
                action: Action.subscribe,
                eventName: eventName,
                resolveId: id
            }) + '\n', () => resolve());
        })



    }
    /**
     * @param {string} eventName 
     * @param {object} value
     */
    raiseEvent(eventName, value) {

        console.log(`Raise on ${eventName} : `, value);

        return new Promise(resolve => {
            this.send(JSON.stringify({
                action: Action.raiseEvent,
                eventName: eventName,
                payload: value
            }) + '\n', () => resolve())
        })

    }



    /**
     * @param {string} value
     * @param {{ (): void; (err?: Error): void; }} [cb]
     */
    send(value, cb) {
        this.waitForSocketConnection(this.innerClient, () => {
            this.innerClient.send(value, cb);
        })
    }

    /**
     * @param {WebSocket} socket
     * @param {{ (): void; (): void; }} callback
     */
    waitForSocketConnection(socket, callback) {
        const self = this;
        setTimeout(
            function () {
                if (socket.readyState === 1) {
                    console.log("Connection is made")
                    if (callback != null) {
                        callback();
                    }
                } else {
                    console.log("wait for connection...")
                    self.waitForSocketConnection(socket, callback);
                }

            }, 30); // wait 5 milisecond for the connection...
    }

}

