//@ts-check

const WebSocket = require("ws");
import Actions, { pureJSAction } from '../src/actions';
import { Server } from 'net';



class SocketClient {

    /**
     * @type {WebSocket}
     */
    socket;

    /**
     * @type {string}
     */
    label;

    static nbClient = 0;


    /**
     * @param {WebSocket} socket
     * @param {string} label
     */
    constructor(socket, label = `Client : ${SocketClient.nbClient++}`) {
        this.socket = socket;
        this.label = label;
    }

    /**
     * @param {string} event
     * @param {any} cb
     */
    on(event, cb) {
        this.socket.on(event, cb);
    }


    /**
     * @return {Promise}
     * @param {*} value 
     */
    send(value) {

        return new Promise((resolve, reject) => {
            this.socket.send(JSON.stringify(value) + "\n", (error) => {
                if (error) {
                    reject(error);
                }
                resolve();
            })
        });

    }
}


/**
 * @abstract
 * @description this class allow you to create a tcp server that emulate Naoqi'q ALMemory System 
 */
class ALMemoryServer {

    /**
     * @description Singloton Instance for Server 
     * @type {any} 
     * */
    static instance

    static clientMap = new Map();

    static ALMemoryMap = new Map();
    /**
     * @description A map where is stored as key the name of an ALMemory and as value a Set of sockets that will be notified when someone raise an event for this ALMemory
     * @type {Map<string, Set<SocketClient>>}
     */
    static eventSocketMap = new Map();

    /**
     * 
     * @param {SocketClient} socket 
     */
    static ack(socket) {
        socket.send({
            action : pureJSAction.jobCompleted
        })
    }


    /**
     * @param {string} host
     * @param {number} port
     */
    static createServer(host, port) {

        console.log("GetInstance");

        if (ALMemoryServer.instance === undefined) {

            ALMemoryServer.instance = new WebSocket.Server({ host: host, port: port })
            /**
             */
            ALMemoryServer.instance.on('connection', socket => {

                const socketClient = new SocketClient(socket)

                /**
                 * @param {any} client
                 */
                socketClient.on("close", (...client) => {
                    console.log("lost connection with", client);
                })

                /**
                 * @param {{ split: (arg0: string) => void; }} data
                 */
                socketClient.on("message", (data) => {

                    const clientDataArr = data.split("\n");
                    /**
                     * @param {string} clientDataRaw
                     */
                    clientDataArr.forEach(clientDataRaw => {


                        if (clientDataRaw.includes('{') && clientDataRaw.includes('}')) {
                            const clientData = JSON.parse(clientDataRaw);
                            console.debug("new Data : ", clientDataRaw);


                            switch (clientData.action) {
                                case Actions.getData:
                                    console.log("GetData");
                                    const ALMemory = ALMemoryServer.ALMemoryMap.get(clientData.ALMemoryName)
                                    let ALMemoryValue = ALMemory === undefined ? null : ALMemory.value;
                                    socketClient.send({
                                        action: Actions.getData,
                                        value: ALMemoryValue,
                                        resolveId: clientData.resolveId
                                    });
                                    break;

                                case Actions.insertData:
                                    console.log("insertData");

                                    if (clientData.ALMemoryName !== undefined && clientData.value !== undefined) {
                                        ALMemoryServer.insertData(clientData);
                                        // ALMemoryServer.sendJobIsCompleted(socket, clientData.resolveId);
                                    }
                                    break;

                                case Actions.subscribe:

                                    console.log("subscribe");

                                    if (clientData.eventName !== undefined) {

                                        if (!ALMemoryServer.eventSocketMap.has(clientData.eventName)) {
                                            ALMemoryServer.eventSocketMap.set(clientData.eventName, new Set())

                                        }

                                        const socketList = ALMemoryServer.eventSocketMap.get(clientData.eventName);
                                        socketList.add(socketClient);

                                        // ALMemoryServer.sendJobIsCompleted(socket, clientData.resolveId)

                                    }
                                    break;

                                case Actions.raiseEvent:
                                    console.log("raiseEvent");

                                    if (clientData.eventName !== undefined) {

                                        ALMemoryServer.insertData(clientData);

                                        const socketList = ALMemoryServer.eventSocketMap.get(clientData.eventName);

                                        if (socketList !== undefined) {
                                            socketList.forEach(socketClient => {
                                                socketClient.send({
                                                    action: Actions.raiseEvent,
                                                    eventName: clientData.eventName,
                                                    payload: clientData.payload,
                                                })
                                            })
                                        }

                                    }
                                    break;

                                default:
                                    break;
                            }

                        }

                    });

                })


            });

        }

        return ALMemoryServer.instance;

    }

  

    /**
     * @description Change data in ALMemory named 'ALMemoryName', **care** it will *not notifies* observers and *will update* timestamp of ALMemory.
     * @param {{ ALMemoryName: String; value: Object; }} clientData
     */
    static insertData(clientData) {
        ALMemoryServer.ALMemoryMap.set(clientData.ALMemoryName, { value: clientData.value, time: Date.now() });
    }

}


export default ALMemoryServer;